﻿using UnityEngine;
using System.Collections;

public class PingPongLight : MonoBehaviour {

    [SerializeField]
    private GameObject[] whiteLights, redLights;
    
	void Update () {
        float floor = 0f;
        float ceiling = 1f;
        float status = floor + Mathf.PingPong(Time.time, ceiling - floor);

        if (status <= 0.1f)
        {
            for (int i = 0; i < whiteLights.Length; i++)
                whiteLights[i].SetActive(false);

            for (int i = 0; i < redLights.Length; i++)
                redLights[i].SetActive(true);
        }
        else if (status >= 0.9f)
        {
            for (int i = 0; i < whiteLights.Length; i++)
                whiteLights[i].SetActive(true);

            for (int i = 0; i < redLights.Length; i++)
                redLights[i].SetActive(false);
        }
	}
}
