﻿using UnityEngine;
using System.Collections;

public class Interactor : MonoBehaviour {

    public float interactRange = 3f;
    public float interactRate = 0.25f;

    private Camera playerCam;
    private Interactable interactObj;
    private float nextInteract;

	void Start () {
        playerCam = GetComponent<Camera>();
	}
	

	void Update () {
	    if (Input.GetButtonDown ("Fire1") && Time.time > nextInteract)
        {
            nextInteract = Time.time + interactRate;

            Vector3 rayOrigin = playerCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0f));
            RaycastHit hit;

            if (Physics.Raycast(rayOrigin, playerCam.transform.forward, out hit, interactRange))
            {
                if (hit.collider.tag == "Door")
                {
                    hit.transform.gameObject.GetComponent<DoorInteraction>().Interact();
                }
                else if (hit.collider.tag == "LightSwitch")
                {
                    hit.transform.gameObject.GetComponent<LightSwitchInteraction>().Interact();
                }
                else if (hit.collider.tag == "Drawer")
                {
                    hit.transform.gameObject.GetComponent<DrawerInteraction>().Interact();
                }
                else if (hit.collider.tag == "Pickup")
                {
                    Debug.Log("Ray hit pickup");
                    hit.transform.gameObject.GetComponent<PickupItem>().Interact();
                }
                else if (hit.collider.tag == "LockedDoor")
                {
                    SoundManager.instance.PlayLockedDoor();
                }
                else if (hit.collider.tag == "Untagged")
                {
                    Debug.Log("No interact behaviour for " + hit.transform.gameObject + " or object has not been tagged");
                }
                else
                {
                    Debug.Log("Name: " + hit.collider.name + ", Tag: " + hit.collider.tag);
                }
            }
            else
            {
                Debug.Log("Object out of range");
            }
        }
	}
    
}
