﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using System.Collections;

public class IntroManager : MonoBehaviour {

    private AudioSource aud;

    void Start()
    {
        aud = GetComponent<AudioSource>();
    }

    public void PlayAudio()
    {
        aud.Play();
    }

    public void EnablePlayerMovement()
    {
        Debug.Log("Setting player speeds");
        FirstPersonController.instance.m_WalkSpeed = 5f;
        FirstPersonController.instance.m_RunSpeed = 10f;
        UIManager.instance.ShowLMBInteract(true);
    }

    public void QuitGame()
    {
        GameManager.instance.Quit();
    }

}
