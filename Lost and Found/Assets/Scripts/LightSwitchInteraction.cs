﻿using UnityEngine;
using System.Collections;

public class LightSwitchInteraction : MonoBehaviour, Interactable {

    [SerializeField]
    private Light lightSource;
    [SerializeField]
    private Animator animator;
    private AudioSource audioSource;

    [SerializeField]
    private Material[] lightShadeOffMats, lightShadeOnMats;
    [SerializeField]
    private Renderer lightShade;

    private bool canInteract;
    private bool switchOff = true;

	// Use this for initialization
	void Start () {
        audioSource = GetComponent<AudioSource>();
	}

    public void Interact()
    {
        Debug.Log("Light Switch Interaction");
        if (switchOff)
        {
            switchOff = false;
            lightSource.gameObject.SetActive(true);
            audioSource.Play();
            animator.SetTrigger("SwitchOn");
            lightShade.materials = lightShadeOnMats;
        }
        else if (!switchOff)
        {
            switchOff = true;
            lightSource.gameObject.SetActive(false);
            audioSource.Play();
            animator.SetTrigger("SwitchOff");
            lightShade.materials = lightShadeOffMats;
        }
    }
}
