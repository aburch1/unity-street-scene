﻿using UnityEngine;
using System.Collections;

public class DrawerInteraction : MonoBehaviour, Interactable {

    [SerializeField]
    private AudioClip openDrawer, closeDrawer;

    private Animator animator;
    private AudioSource audioSource;

    private bool canInteract;
    private bool drawerOpen = false;

    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }

    public void Interact()
    {
        if (drawerOpen)
        {
            animator.SetTrigger("CloseDrawer");
            audioSource.clip = closeDrawer;
            audioSource.Play();
        }
        else
        {
            animator.SetTrigger("OpenDrawer");
            audioSource.clip = openDrawer;
            audioSource.Play();
        }
        drawerOpen = !drawerOpen;
    }
}