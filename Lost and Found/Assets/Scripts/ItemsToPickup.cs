﻿using UnityEngine;
using System.Collections;

public class ItemsToPickup : MonoBehaviour {

    public static ItemsToPickup instance;

    private bool hasMedicalClipboard = false;
    private bool hasBeenToldToCheckHouses = false;
    private bool hasCheckedHouses = false;
    public int numOfHousesChecked = 0;
    private bool hasNoticedMainHouse = false;
    private bool hasHouseKey = false;
    private bool hasTVStatic = false;
    private bool hasSpilledPills = false;
    private bool hasBeenToldToCheckNewspaper = false;
    private bool hasNewspaper = false;

    void Start()
    {
        if (instance == null)
            instance = this;
    }

    public bool GetMedicalClipboardStatus()
    {
        return hasMedicalClipboard;
    }

    public void PickupMedicalClipboard()
    {
        hasMedicalClipboard = true;
    }

    public bool GetBeenToldToCheckHousesStatus()
    {
        return hasBeenToldToCheckHouses;
    }

    public void PickupBeenToldToCheckHouses()
    {
        hasBeenToldToCheckHouses = true;
    }

    public bool GetCheckedHousesStatus()
    {
        return hasCheckedHouses;
    }

    public void PickupCheckHouse()
    {
        numOfHousesChecked++;
        if (numOfHousesChecked >= 3)
            hasCheckedHouses = true;
    }

    public bool GetHasNoticedMainHouseStatus()
    {
        return hasNoticedMainHouse;
    }

    public void PickupHasNoticedMainHouse()
    {
        hasNoticedMainHouse = true;
    }

    public bool GetHouseKeyStatus()
    {
        return hasHouseKey;
    }

    public void PickupHouseKey()
    {
        hasHouseKey = true;
    }

    public bool GetTVStaticStatus()
    {
        return hasTVStatic;
    }

    public void PickupTVStatic()
    {
        hasTVStatic = true;
    }

    public bool GetSpilledPillsStatus()
    {
        return hasSpilledPills;
    }

    public void PickupSpilledPills()
    {
        hasSpilledPills = true;
    }

    public bool GetBeenToldToCheckNewspaperStatus()
    {
        return hasBeenToldToCheckNewspaper;
    }

    public void PickupBeenToldToCheckNewspaper()
    {
        hasBeenToldToCheckNewspaper = true;
    }

    public bool GetNewspaperStatus()
    {
        return hasNewspaper;
    }

    public void PickupNewspaper()
    {
        hasNewspaper = true;
    }
}
