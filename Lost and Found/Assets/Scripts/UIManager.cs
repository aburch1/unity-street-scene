﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIManager : MonoBehaviour {

    public static UIManager instance = null;

    [SerializeField]
    private GameObject[] pauseElements;

    [SerializeField]
    private Animator blindPlayerAnim;

    [SerializeField]
    private GameObject LMBToInteractImage;

	// Use this for initialization
	void Start () {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void ShowPauseMenu(bool status)
    {
        if (status)
        {
            for (int i = 0; i < pauseElements.Length; i++)
            {
                pauseElements[i].SetActive(true);
            }
        }
        else
        {
            for (int i = 0; i < pauseElements.Length; i++)
            {
                pauseElements[i].SetActive(false);
            }
        }
    }

    public void FadeOutGame()
    {
        blindPlayerAnim.SetTrigger("FadeToBlack");
    }

    public void ShowLMBInteract(bool status)
    {
        LMBToInteractImage.SetActive(status);
    }
}
