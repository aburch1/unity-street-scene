﻿using UnityEngine;
using System.Collections;

public class EventInteractable : MonoBehaviour {

    private enum EventType {TOLDTOCHECKHOUSES, NOTICEDMAINHOUSE, CHECKNEWSPAPER}

    [SerializeField]
    private EventType eventType;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            gameObject.SetActive(false);
            Debug.Log("Event");
            if (eventType == EventType.TOLDTOCHECKHOUSES)
            {
                ItemsToPickup.instance.PickupBeenToldToCheckHouses();
                SoundManager.instance.PlayPlayerCheckHouses();
            }
            else if (eventType == EventType.NOTICEDMAINHOUSE)
            {
                ItemsToPickup.instance.PickupHasNoticedMainHouse();
                SoundManager.instance.PlayNoticedMainHouse();
            }
            else if (eventType == EventType.CHECKNEWSPAPER)
            {
                ItemsToPickup.instance.PickupBeenToldToCheckNewspaper();
                SoundManager.instance.PlayCheckNewspaper();
            }
        }
    }
}
