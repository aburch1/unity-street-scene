﻿using UnityEngine;
using System.Collections;

public class DoorLocked : MonoBehaviour {

    private AudioSource audioSource;

    private bool canInteract;

	// Use this for initialization
	void Start () {
        audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        if (canInteract && Input.GetButtonDown("Fire1"))
        {
            audioSource.Play();
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            canInteract = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            canInteract = false;
        }
    }
}
