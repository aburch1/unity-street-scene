﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameManager : MonoBehaviour {

    public static GameManager instance = null;

    private bool cursorToggle = false;

    public bool gamePaused = false;
    public bool pauseMenuOpen = false;

    [SerializeField]
    private GameObject medicalClipboard, toldToCheckHouses, houseCheck1, houseCheck2, houseCheck3, noticedMainHouse, houseKey, tvStatic, spilledPills, checkNewspaper, newspaper;
    
	void Start () {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
	}
	
	void Update () {
	    if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (pauseMenuOpen)
                ShowPauseMenu(false);
            else
                ShowPauseMenu(true);
        }

        if (ItemsToPickup.instance.GetMedicalClipboardStatus())
        {
            if (ItemsToPickup.instance.GetBeenToldToCheckHousesStatus())
            {
                if (ItemsToPickup.instance.GetCheckedHousesStatus())
                {
                    if (ItemsToPickup.instance.GetHasNoticedMainHouseStatus())
                    {
                        if (ItemsToPickup.instance.GetHouseKeyStatus())
                        {
                            if (ItemsToPickup.instance.GetTVStaticStatus())
                            {
                                if (ItemsToPickup.instance.GetSpilledPillsStatus())
                                {
                                    if (ItemsToPickup.instance.GetBeenToldToCheckNewspaperStatus())
                                    {
                                        if (ItemsToPickup.instance.GetNewspaperStatus())
                                        {
                                            Debug.Log("The Secret has been found!");
                                        }
                                        else
                                        {
                                            newspaper.SetActive(true);
                                        }
                                    }
                                    else
                                    {
                                        checkNewspaper.SetActive(true);
                                    }
                                }
                                else
                                {
                                    spilledPills.SetActive(true);
                                }
                            }
                            else
                            {
                                tvStatic.SetActive(true);
                            }
                        }
                        else
                        {
                            houseKey.SetActive(true);
                        }
                    }
                    else
                    {
                        noticedMainHouse.SetActive(true);
                    }
                }
                else
                {
                    if (ItemsToPickup.instance.numOfHousesChecked <= 0)
                    {
                        houseCheck1.SetActive(true);
                        houseCheck2.SetActive(true);
                        houseCheck3.SetActive(true);
                    }
                }
            }
            else
            {
                toldToCheckHouses.SetActive(true);
            }
        }
        else
        {
            medicalClipboard.SetActive(true);
        }
	}

    private void PauseGame(bool paused)
    {
        if (paused)
        {
            Time.timeScale = 0f;
            gamePaused = true;
        }
        else
        {
            Time.timeScale = 1f;
            gamePaused = false;
        }
    }

    public void ShowPauseMenu(bool status)
    {
        UIManager.instance.ShowPauseMenu(status);
        pauseMenuOpen = status;
        PauseGame(status);
    }

    public void EndGame()
    {
        UIManager.instance.FadeOutGame();
    }

    public void Quit()
    {
        SceneManager.LoadScene("Main Menu");
    }
}
