﻿using UnityEngine;
using System.Collections;

public class DoorInteraction : MonoBehaviour, Interactable
{
    [SerializeField]
    private Light fridgeLight;
    [SerializeField]
    private bool audioStartStopMode;

    [SerializeField]
    private bool needsKey;

    private Animator animator;
    private AudioSource audioSource;

    private bool canInteract;
    private bool doorOpen = false;

    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }

    public void Interact()
    {
        if (doorOpen)
        {
            if(needsKey)
            {
                if (ItemsToPickup.instance.GetHouseKeyStatus())
                {
                    animator.SetTrigger("CloseDoor");

                    if (audioStartStopMode)
                        audioSource.Stop();
                    else
                        audioSource.Play();
                    doorOpen = !doorOpen;
                    if (fridgeLight != null)
                        fridgeLight.gameObject.SetActive(false);
                }
                else
                {
                    SoundManager.instance.PlayLockedDoor();
                }
            }
            else
            {
                animator.SetTrigger("CloseDoor");
                if (audioStartStopMode)
                    audioSource.Stop();
                else
                    audioSource.Play();
                doorOpen = !doorOpen;
                if (fridgeLight != null)
                    fridgeLight.gameObject.SetActive(false);
            }
        }
        else
        {
            if (needsKey)
            {
                if (ItemsToPickup.instance.GetHouseKeyStatus())
                {
                    animator.SetTrigger("OpenDoor");
                    audioSource.Play();
                    doorOpen = !doorOpen;
                    if (fridgeLight != null)
                        fridgeLight.gameObject.SetActive(true);
                }
                else
                {
                    SoundManager.instance.PlayLockedDoor();
                }
            }
            else
            {
                animator.SetTrigger("OpenDoor");
                audioSource.Play();
                doorOpen = !doorOpen;
                if (fridgeLight != null)
                    fridgeLight.gameObject.SetActive(true);
            }
        }
    }
}