﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class SoundManager : MonoBehaviour {

    public static SoundManager instance = null;

    private AudioSource ambientWind;
    private AudioSource mainMenuMusic;

    private AudioSource player_medicalReport;
    private AudioSource player_checkHousesForHelp;
    private AudioSource player_houseCheck1;
    private AudioSource player_houseCheck2;
    private AudioSource player_houseCheck3;
    private AudioSource player_noticedMainHouse;
    private AudioSource player_tvStatic;
    private AudioSource player_spilledPills;
    private AudioSource player_checkNewspaper;
    private AudioSource player_readNewspaper;

    private AudioSource lockedDoor;
    private AudioSource paperRustle;

    // Use this for initialization
    void Start () {

        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

        ambientWind = transform.GetChild(0).GetComponent<AudioSource>();
        mainMenuMusic = transform.GetChild(1).GetComponent<AudioSource>();
        player_medicalReport = transform.GetChild(2).GetComponent<AudioSource>();
        player_checkHousesForHelp = transform.GetChild(3).GetComponent<AudioSource>();
        player_houseCheck1 = transform.GetChild(4).GetComponent<AudioSource>();
        player_houseCheck2 = transform.GetChild(5).GetComponent<AudioSource>();
        player_houseCheck3 = transform.GetChild(6).GetComponent<AudioSource>();
        player_noticedMainHouse = transform.GetChild(7).GetComponent<AudioSource>();
        player_tvStatic = transform.GetChild(8).GetComponent<AudioSource>();
        player_spilledPills = transform.GetChild(9).GetComponent<AudioSource>();
        player_checkNewspaper = transform.GetChild(10).GetComponent<AudioSource>();
        player_readNewspaper = transform.GetChild(11).GetComponent<AudioSource>();
        lockedDoor = transform.GetChild(12).GetComponent<AudioSource>();
        paperRustle = transform.GetChild(13).GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
        if (SceneManager.GetActiveScene().name == "Main Menu")
        {
            if (ambientWind.isPlaying)
            {
                ambientWind.Stop();
            }

            if (!mainMenuMusic.isPlaying)
            {
                mainMenuMusic.Play();
            }
        }
        else if (SceneManager.GetActiveScene().name == "Game")
        {
            if (!ambientWind.isPlaying)
            {
                ambientWind.Play();
            }

            if (mainMenuMusic.isPlaying)
            {
                mainMenuMusic.Stop();
            }
        }
	}

    public void PlayPlayerMedicalReport()
    {
        player_medicalReport.Play();
    }

    public void PlayPlayerCheckHouses()
    {
        player_checkHousesForHelp.Play();
    }
    public void PlayHouseCheck1()
    {
        player_houseCheck1.Play();
    }

    public void PlayHouseCheck2()
    {
        player_houseCheck2.Play();
    }
    public void PlayHouseCheck3()
    {
        player_houseCheck3.Play();
    }

    public void PlayNoticedMainHouse()
    {
        player_noticedMainHouse.Play();
    }

    public void PlayTVStatic()
    {
        player_tvStatic.Play();
    }

    public void PlaySpilledPills()
    {
        player_spilledPills.Play();
    }
    public void PlayCheckNewspaper()
    {
        player_checkNewspaper.Play();
    }

    public void PlayReadNewspaper()
    {
        player_readNewspaper.Play();

        Invoke("EndGame", player_readNewspaper.clip.length + 3f);
    }

    public void PlayLockedDoor()
    {
        lockedDoor.Play();
    }

    public void PlayPaperRustle()
    {
        paperRustle.Play();
    }

    private void EndGame()
    {
        GameManager.instance.EndGame();
    }

}
