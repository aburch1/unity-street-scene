﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Characters.FirstPerson;

public class GroundSounds : MonoBehaviour {

    public List<GroundType> GroundTypes = new List<GroundType>();
    public FirstPersonController fpc;
    public string currentGround;

	// Use this for initialization
	void Start () {
        setGroundType(GroundTypes[0]);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.transform.tag == "WoodFloor" || hit.transform.tag == "Pickup")
        {
            setGroundType(GroundTypes[1]);
        }
        else
        {
            setGroundType(GroundTypes[0]);
        }
    }

    public void setGroundType(GroundType ground)
    {
        if (currentGround != ground.name)
        {
            fpc.m_FootstepSounds = ground.footstepSounds;
            currentGround = ground.name;
        }
    }
}

[System.Serializable]
public class GroundType
{
    public string name;

    public AudioClip[] footstepSounds;
}
