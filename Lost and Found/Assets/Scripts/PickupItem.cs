﻿using UnityEngine;
using System.Collections;

public class PickupItem : MonoBehaviour, Interactable {
    
    private enum ItemType {MEDICAL_CLIPBOARD, HOUSECHECK, HOUSE_KEY, TVSTATIC, NEWSPAPER, SPILLED_PILLS}

    [SerializeField]
    private ItemType itemType;

    public void Interact()
    {
        gameObject.SetActive(false);

        if (itemType == ItemType.MEDICAL_CLIPBOARD)
        {
            ItemsToPickup.instance.PickupMedicalClipboard();
            SoundManager.instance.PlayPlayerMedicalReport();
            UIManager.instance.ShowLMBInteract(false);
        }
        else if (itemType == ItemType.HOUSECHECK)
        {
            ItemsToPickup.instance.PickupCheckHouse();
            if (ItemsToPickup.instance.numOfHousesChecked == 1)
                SoundManager.instance.PlayHouseCheck1();
            else if (ItemsToPickup.instance.numOfHousesChecked == 2)
                SoundManager.instance.PlayHouseCheck2();
            else if (ItemsToPickup.instance.numOfHousesChecked == 3)
                SoundManager.instance.PlayHouseCheck3();
        }
        else if (itemType == ItemType.HOUSE_KEY)
        {
            ItemsToPickup.instance.PickupHouseKey();
        }
        else if (itemType == ItemType.TVSTATIC)
        {
            ItemsToPickup.instance.PickupTVStatic();
            SoundManager.instance.PlayTVStatic();
        }
        else if (itemType == ItemType.NEWSPAPER)
        {
            ItemsToPickup.instance.PickupNewspaper();
            SoundManager.instance.PlayPaperRustle();
            SoundManager.instance.PlayReadNewspaper();
        }
        else if (itemType == ItemType.SPILLED_PILLS)
        {
            ItemsToPickup.instance.PickupSpilledPills();
            SoundManager.instance.PlaySpilledPills();
        }
    }
}
