﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MenuManager : MonoBehaviour {

    private Animator anim;

    public void Start()
    {
        anim = GetComponent<Animator>();
    }

    public void StartButton()
    {
        Debug.Log("Start Pressed");
        anim.SetTrigger("FadeOut");
    }

    public void StartGame()
    {
        SceneManager.LoadScene("Game");
    }

    public void ConfirmQuit()
    {
        anim.SetTrigger("ConfirmQuit");
    }

    public void NoQuitGame()
    {
        anim.SetTrigger("NoQuit");
    }

    public void YesQuitGame()
    {
        Application.Quit();
    }

    public void QuitToMenu()
    {
        SceneManager.LoadScene("Main Menu");
    }

    public void ResumeGame()
    {
        if (GameManager.instance.gamePaused && GameManager.instance.pauseMenuOpen)
        {
            GameManager.instance.ShowPauseMenu(false);
        }
    }
}
